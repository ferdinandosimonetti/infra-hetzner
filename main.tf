# SSH key per il project
module "projectkey" {
  source  = "bitbucket.org/ferdinandosimonetti/tf-modules//hetznersshpubkey"
  keyfile = var.keyfile
  keyname = var.keyname
}
# Ansible host
module "ansible" {
  source  = "bitbucket.org/ferdinandosimonetti/tf-modules//hetznerserver"
  name = "ansible00" 
  server_type = var.defaultservertype
  image = var.defaultimage
  userdatafile = var.bastionhostuserdatafile 
  sshkeysnames = [ module.projectkey.name ]
  labels = { role = "docker" }
}
# Record A per Ansible host
module "ansiblehostdns" {
  source   = "bitbucket.org/ferdinandosimonetti/tf-modules//route53recordA"
  zonename = var.dnszonename
  src      = module.ansible.name
  dst      = [ module.ansible.address ]
  awsregion = var.awsregion
}
