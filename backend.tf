terraform {
  required_version = "~> 1.0.1"
  backend "remote" {
    organization = "fsimonetti-eu" # org name from step 2.
    workspaces {
      name = "infra-hetzner" # name for your app's state.
    }
  }
}